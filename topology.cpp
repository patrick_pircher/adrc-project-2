#include "topology.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include "foreach.h"
#include <queue>



Topology::Topology()
{

}

Topology::~Topology()
{
	typedef std::pair<int, Node*> key_value;
	foreach(const key_value pair, network){

		delete pair.second;
	}
}

bool Topology::load(char *filename)
{
	std::string line;
	std::ifstream myfile(filename);


	if (myfile.is_open())
	{
		std::map<std::pair<int,int>,LinkGroup*> links;

		while( std::getline(myfile,line) )
		{
			std::istringstream iss(line);
			int tail, head;
			int type;
			iss >> tail >> head >> type;

			LinkGroup* link = links[std::make_pair(tail,head)];
			if(link==NULL){

				Node* node   = this->network[tail];
				Node* target = this->network[head];

				if( node==NULL ){
					node = new Node;
					this->network[tail] = node;
				}

				if( target==NULL ){
					target = new Node;
					this->network[head] = target;
				}

				node->id = tail;
				target->id = head;

				link = new LinkGroup;
				links[std::make_pair(tail,head)] = link;
				links[std::make_pair(head,tail)] = link;
				link->n1 = node;
				link->n2 = target;

				node->links.push_back(link);
				target->links.push_back(link);
			}

			link->_type[head] = (Type)type;
			switch (type) {
				case Peer:
					link->_type[tail] = Peer;
					break;
				case Provider:
					link->_type[tail] = Customer;
					break;
				case Customer:
					link->_type[tail] = Provider;
					break;
			}
		}
		myfile.close();
		return true;

	}else{

		std::cout << "Unable to open file";
		return false;

	}

}

std::vector<Topology::Triple> Topology::electedRoutes(Topology::Node *dest)
{
	std::list<Node*> customers, peers, providers;
	dest->elected = Start;
	int count=0;

	//send packets
	foreach(LinkGroup* link, dest->links){

		Node* target = link->target(dest);
		target->elected = link->type(target);
		count++;

		switch(link->type(target))
		{
		case Customer:
			customers.push_back(target);
			break;
		case Peer:
			peers.push_back(target);
			break;
		case Provider:
			providers.push_back(target);
			break;
		default:
			break;
		}

	}

	while( !customers.empty() || !peers.empty() || !providers.empty() ){

		Type fromType;
		Node* node;
		std::list<Node*> *list;

		if(!customers.empty()){
			list = &customers;
			fromType = Customer;
		}else if(!peers.empty()){
			list = &peers;
			fromType = Peer;
		}else if(!providers.empty()){
			list = &providers;
			fromType = Provider;
		}

		node = list->front();
		list->pop_front();


		foreach(LinkGroup* link, node->links){

			Node* target = link->target(node);

			switch(fromType){


			case Provider:
				if( link->type(node)!=Customer ){
					continue;
				}
				break;
			case Peer:
				if( link->type(node)!=Customer ){
					continue;
				}
				break;
			case Customer:
				break;
			default:
				break;
			}


			if(!target->elected || (target->elected < link->type(target) )) {

				target->elected = link->type(target);
				count++;

				switch(link->type(target))
				{

				case Customer:
					customers.push_back(target);
					break;
				case Peer:
					peers.push_back(target);
					break;
				case Provider:
					providers.push_back(target);
					break;
				default:
					break;
				}

			}
		}
	}

	std::vector<Triple> triples(count);
	typedef std::pair<int, Node*> key_value;
	foreach(key_value kv, this->network){

		Node* node = kv.second;
		if(node->elected){
			Triple triple = {node->id,dest->id,node->elected};
			triples.push_back(triple);
		}
	}
	this->clean();
	return triples;
}

void Topology::electedRoutesStats()
{
	typedef std::pair<int, Node*> key_value;
	int total = this->network.size();
	total = total * (total-1);
	int nCustomer = 0;
	int nPeer = 0;
	int nProvider = 0;
	int nDisconnected = 0;
	int count = 0;

	foreach(const key_value kv, network){

		std::vector<Triple> triples = this->electedRoutes(kv.second);
		std::cout << "\r" << 100.*count++/network.size() << "%" ;
		foreach (Triple triple, triples) {

			switch(triple.type){
			case(Customer):
				nCustomer++;
				break;
			case(Peer):
				nPeer++;
				break;
			case(Provider):
				nProvider++;
				break;
			default:
				break;
			}
		}
	}

	nDisconnected += total - nCustomer - nPeer - nProvider;

	std::cout << std::endl;
	std::cout << "reaches over customer route: " << 100.0*nCustomer/total << "%" << std::endl;
	std::cout << "reaches over peer route: " << 100.0*nPeer/total << "%" << std::endl;
	std::cout << "reaches over provider route: " << 100.0*nProvider/total << "%" << std::endl;
	std::cout << "does not reach: " << 100.0 * nDisconnected/total << "%" << std::endl;
	std::cout << std::endl;
}


bool Topology::isReachableByAll(Node* node){

	std::vector<Triple> list = this->electedRoutes(node);
	if(list.size()!=network.size()-1){
		return false;
	}
	return true;
}

bool Topology::policyConnected()
{
	typedef std::pair<int, Node*> key_value;
	foreach(const key_value kv, network){

		if(this->electedRoutes(kv.second).size()!=network.size()-1){
			return false;
		}
		this->clean();
	}
	return true;
}

void Topology::printNetwork()
{
	typedef std::pair<int, Node*> key_value;
	foreach(const key_value pair, network){

		foreach(LinkGroup* link, pair.second->links){

			std::cout << pair.second->id << " " ;
			std::cout << link->target((Node*)pair.second)->id << " ";
			std::cout << link->type((Node*)pair.second);
			std::cout << std::endl;
		}
	}
}

void Topology::clean()
{
	typedef std::pair<int, Node*> key_value;
	foreach(const key_value pair, network){

		pair.second->elected = None;
	}
}

Topology::Node::Node()
	:visited(0), elected(None)
{}

Topology::Type Topology::LinkGroup::type(Node *n)
{
	return _type[n->id];
}

Topology::Node *Topology::LinkGroup::target(Node *n)
{
	return n->id==this->n1->id?n2:n1;
}
