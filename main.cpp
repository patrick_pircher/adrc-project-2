#include <iostream>
#include "topology.h"

int main(int argc, char** argv)
{
	if(argc<2){
		std::cout << "Not enough parameters";
		return 0;
	}
	char answer;
	Topology topology;
	bool ok = topology.load(argv[1]);
	if(!ok){
		return 0;
	}
	std::cout << "loaded" << std::endl;

	while (answer!='p' && answer!='e') {
		std::cout << "choose:" << std::endl;
		std::cout << "elected routes [e]" << std::endl;
		std::cout << "policy connected [p]" << std::endl;
		std::cin >> answer;
	}

	std::cout.setf( std::ios::fixed, std:: ios::floatfield );
	std::cout.precision(3);
	if(answer=='e'){

		std::cout << "processing elected routes stats..." << std::endl;
		topology.electedRoutesStats();
	}

	if(answer=='p'){

		std::cout << "processing policy connected..." << std::endl;
		bool connected = topology.policyConnected();
		std::cout << "connected: " << (connected?"true":"false") << std::endl;
	}

	return 1;
}
