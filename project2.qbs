import qbs 1.0

Application {

    name: 'inter-domain-routing'

    Group {
        name: "Sources"
        files: [
            "*.cpp"
        ]
        Depends { name:"cpp" }
    }

    Group {
        name: "Headers"
        files: [
            "*.h"
        ]
    }
    Depends { name:"cpp" }
    cpp.linkerFlags: [
        "-static",
    ]

}


