#ifndef TOPOLOGY_H
#define TOPOLOGY_H

#include <list>
#include <vector>
#include <map>
#include <iostream>

class Topology
{
		struct Node;
		typedef enum{
			None,
			Provider,
			Peer,
			Customer,
			Start
		}Type;
		typedef struct LinkGroup
		{
				std::map<int, Type> _type;

				Type type(Node* n);
				Node* target(Node* n);

				Node* n1;
				Node* n2;
		}LinkGroup;

		typedef struct Node
		{
				int id;
				std::list<LinkGroup*> links;
				bool visited;
				Type elected;
				Node();
		}Node;

		typedef struct Package{
				Node* node;
				int priority;
				Type fromType;
				bool operator<(const Package &other) const{
					return this->priority>other.priority;
				}
		}Package;

	public:

		typedef struct Triple{
			int source;
			int dest;
			Type type;
		}Triple;

		Topology();
		~Topology();
		bool load(char* filename);
		std::vector<Triple> electedRoutes(Node* dest);
		void electedRoutesStats();
		bool policyConnected();
		void printNetwork();

	private:
		std::map<int, Topology::Node*> network;

		void clean();
		bool isReachableByAll(Node *node);
};

#endif // TOPOLOGY_H
